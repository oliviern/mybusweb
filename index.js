//Import core node modules
var express = require('express');
var http = require('http');
var mysql = require('mysql');
var bodyParser = require('body-parser');

const Buffer = require('buffer').Buffer;
var Promise = require('promise');
var app = express();

const fs = require('fs');

// Tell the bodyparser middleware to accept more data
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

//Used for parsing form data
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//View Engine --- Template parsing with EJS types
app.set('view engine', 'ejs');

//Setup Port for Heroku Deployment
app.set('port', (process.env.PORT || 5000));

// Import all JavaScript and CSS files for application
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/js', express.static(__dirname + '/node_modules/tether/dist/js'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use(express.static(__dirname + '/public'));


//Assign site constants
const siteTitle = "CRUD APP";
const baseURL = "/";
//const baseURL = "http://localhost:" + app.get('port') + "/";
const TMDB_API_KEY = "244a059cc1db5224bab95119b674815b"; //Oh no don't steal my api key!! D:
const TMDB_BasePoster = 'https://image.tmdb.org/t/p/w185_and_h278_bestv2/';

//Use TheMovieDB API to pull information about movies
// const TheMovieDB = require('moviedb')(TMDB_API_KEY);



//Database connection details for Heroku and ClearDB
const db_config = {
    host: "localhost",
    user: "root",
    password: "Qjafa1nu!",
	database: "BUS"
};

var dbConnection;
handleDisconnect();


//Setup Database connection and handle timeout problems
function handleDisconnect(){
    dbConnection = mysql.createConnection(db_config);
    console.log("Successfully connected to Database.");
    dbConnection.connect(function(err){
        if(err){
            console.log('Error when connecting to DB: ', err);
            setTimeout(handleDisconnect, 5000);
        }
    });
    dbConnection.on('error', function(err){
        console.log('DB ERROR ', err);
        if(err.code === 'PROTOCOL_CONNECTION_LOST'){
            console.log('Lost connection. Reconnecting...');
            handleDisconnect();
        }
        else{
            throw err;
        }
    });
}



// Connect to Server
var server = app.listen(app.get('port'), function(){
	console.log("Server listening on " + baseURL + " ...");
});



//Query the Movies Table for all movies
function getMovies(){
    return new Promise(function(resolve, reject){
        dbConnection.query("SELECT * FROM movies ORDER BY movieID DESC", function(err, rows, fields){
            if(err){
                console.log("Error loading from DB");
                return reject(err);
            }
            else{
                return resolve(rows);
            }
        });
    });
}



//Load TMDB image path and overview using movieID
/* function loadDataTMDB(movieID){
    console.log("Fetching TMDB Data for movieID: " + movieID);
    return new Promise(function(resolve, reject){
        TheMovieDB.movieInfo({id: movieID}, function(err, result){
            if(err){
                return reject(err);
            }
            else{
                return resolve([(TMDB_BasePoster + result.poster_path), result.overview]);
            }
        });
    });
} */



//Load All TMDB image paths and overviews using Promises
/* function loadAllDataTMDB(rows){
    var images = [];
    var overviews = [];
    var promises = [];
    console.log("Fetching data from TMDB.");

    return new Promise(function(resolve, reject){
        rows.forEach(function(row, index){
            promises.push(loadDataTMDB(row.TMDB_ID).then(function(data){
                images[index] = data[0];
                overviews[index] = data[1];
                console.log("Found image: " + data[0] + " for ID: " + row.TMDB_ID);
            }));
        });

        if(promises.length == rows.length){
            console.log("All promises have been added.");

            Promise.all(promises).then(function(data, err){
                console.log(data);
                if(err){
                    console.log("All TMDB Data Promises were not fulfilled!");
                    return reject(err);
                }
                else{
                    console.log("All TMDB Data Promises were fulfilled!");
                    return resolve([images, overviews]);
                }
            });
        }
    });
} */



//Load default page and list DVDs from mySQL DB
app.get('/', function (request, response){
    console.log("Got to Index.");
    response.render('index', {
                siteTitle : siteTitle,
                pageTitle : "Movies",
                movies : null
            });
    /* getMovies().then(function(rows){
        if(rows.length == 0){
            console.log("Table is empty. Rendering Page...");
            response.render('pages/index.ejs', {
                siteTitle : siteTitle,
                pageTitle : "Movies",
                movies : null
            });
        }
        else{
            console.log("Successfully loaded data from DB.");
            loadAllDataTMDB(rows).then(function(data, err){
                if(err){
                    throw err;
                }
                else{
                    response.render('pages/index.ejs', {
                        siteTitle : siteTitle,
                        pageTitle : "Movies",
                        movies : rows,
                        images : data[0],
                        overviews : data[1]
                    });
                }
            }).catch(function(e){
                console.log(e.stack);
            });
        }
    }).catch(function(e){
        console.log(e.stack);
    });i*/
});

app.get('/dashboard', function (request, response){
    response.render('dashboard', {
    });
});

app.get('/blog', function (request, response){
    response.render('blog', {
    });
});

app.get('/singleblog', function (request, response){
    response.render('single-blog', {
    });
});

app.get('/search', function (request, response){
    response.render('search', {
    });
});

app.get('/login', function (request, response){
    response.render('login', {
    });
});

// app.get('/dashboardarchivedads', function (request, response){
//     response.render('dashboard-archived-ads', {
//     });
// });
//
// app.get('/dashboardfavouritedads', function (request, response){
//     response.render('dashboard-favourite-ads', {
//     });
// });
//
// app.get('/dashboardmydads', function (request, response){
//     response.render('dashboard-my-ads', {
//     });
// });
//
// app.get('/dashboardpendingdads', function (request, response){
//     response.render('dashboard-pending-ads', {
//     });
// });

app.get('/single', function (request, response){
    response.render('single', {
    });
});

app.get('/user', function (request, response){
    response.render('user-profile', {
    });
});

app.get('/at/:idthx', function (request, response){
    response.render('user-profile', {
    });
});

//
// //Display form to Search for Movie Entry to Add
// app.get('/dvd/add', function (request, response){
//     response.render('pages/add-dvd-search.ejs', {
//         siteTitle : siteTitle,
//         pageTitle : "Search For Movie",
//         movies : null,
//     });
// });



// //Add selected DVD  to DB
// app.post('/dvd/add/:movieID', function(request, response){
//
//     var id = ("" + request.params.movieID).substring(1, request.params.movieID.length);
//
//     TheMovieDB.movieInfo({id: id}, function(err, result){
//
//         var query = "INSERT INTO movies (title, genre, rating, " +
//             "year, watched, TMDB_ID, userID" +
//         ")";
//
//         userID = 1; // debug since session handling isnt here yet
//
//         query += " VALUES (";
//             query += " '" + result.title + "',";
//             query += " 'UNIMPLEMENTED',";   //NOT IMPLEMENTED!
//             query += " '" + result.vote_average + "',";
//             query += " '" + result.release_date + "',";
//             query += " " + "0" + ",";
//             query += " '" + id + "',";
//             query += " (SELECT userID FROM users WHERE userID = " + userID + ")"
//         query += ")";
//
//         console.log("[ADDING ENTRY] Query  :\n" + query);
//
//         dbConnection.query(query, function(err, result){
//             if(err) throw err;
//             response.redirect(baseURL);
//         });
//     });
// });



// //Search for entries of Movies from TMDB
// app.post('/dvd/add', function(request, response){
//
//     /*NOTE: Due to limited queries/second from TMDB's API,
//         this will only show the first 20 entries. Implementing pages
//         would be a workaround for this.
//     */
//
//     TheMovieDB.searchMovie({query: request.body.title }, function(err, result){
//         if(result != null && result.total_results >= 1){
//             //console.log(res.results);
//             console.log("[ADDING ENTRY] Found " + result.total_results + " results.");
//             console.log("[ADDING ENTRY] Loading search results...");
//             response.render('pages/add-dvd-search.ejs', {
//                 siteTitle : siteTitle,
//                 pageTitle : "Select Movie",
//                 movies : result.results,
//             });
//         }
//         else{
//             console.log("[ADDING ENTRY] No search results found.");
//             response.redirect(baseURL + 'dvd/add');
//         }
//     });
// });



//Display form to edit movie entry
// app.get('/dvd/edit/:movieID', function(request, response){
//     dbConnection.query("SELECT * FROM movies WHERE movieID = '" + request.params.movieID + "'",
//     function(err,result){
//         response.render('pages/edit-dvd.ejs',{
//             siteTitle : siteTitle,
//             pageTitle : "Editing Movie : " + result[0].title,
//             movie : result
//         });
//     });
// });



app.get('/users', function(request, response){
    dbConnection.query("SELECT pseudo,firstname,lastname,email,publickey FROM users",
    function(err,result){
	response.json(result);
    });
});

app.get('/items', function(request, response){
    dbConnection.query("SELECT id,3 as type, title,content,price,longitude,latitude,(SELECT publickey FROM users WHERE id=o.owner_id) as owner FROM offers o",
    function(err,result){
        response.json(result);
    });
});

app.get('/places', function(request, response){
    dbConnection.query("SELECT id,2 as type, title,content,longitude,latitude,(SELECT publickey FROM users WHERE id=p.owner_id) as owner FROM places p",
    function(err,result){
        response.json(result);
    });
});

app.post('/items/add/:pk', function(request, response){
  console.log("[ADDING ENTRY]");

  var result = request.body;

  var query = "INSERT INTO offers (title, content, owner_id, picture1, price, active )";

  userID = 1; // debug since session handling isnt here yet

  query += " VALUES (";
      query += " '" + result.title + "',";
      query += " '" + result.content + "',";
      query += " (SELECT id FROM users WHERE publickey='" + request.params.pk + "'),"
      query += "'"+result.picture + "',";
      query += result.price + ",";
      query += result.active ;

  query += ")";

  console.log("[ADDING ENTRY] Query  :\n" + query);

  dbConnection.query(query, function(err, result){
      if(err) throw err;
      response.redirect(baseURL);
  });
});

app.get('/items/:pk', function(request, response){
    query =  "SELECT id,title,3 as type,content,price,longitude,latitude,'"+request.params.pk+"' as owner FROM offers";
    query += " WHERE owner_id=(SELECT id FROM users WHERE publickey='"+request.params.pk+"')";
    dbConnection.query(query,
    function(err,result){
        response.json(result);
    });
});

app.get('/places/:pk', function(request, response){
    query =  "SELECT id,title,2 as type,content,longitude,latitude,'"+request.params.pk+"' as owner FROM places";
    query += " WHERE owner_id=(SELECT id FROM users WHERE publickey='"+request.params.pk+"')";
    dbConnection.query(query,
    function(err,result){
        response.json(result);
    });
});

app.get('/item/:id.png', function(request, response){

  console.log("ID IMG="+request.params.id);

  if (request.params.id==undefined) response.json('no ID');

  dbConnection.query("SELECT picture1 FROM offers WHERE id="+request.params.id,
  function(err,result){
    if (result==undefined) return;
    if (result.length!=1) return;

    let buf = new Buffer(result[0].picture1.toString(), 'base64'); //Buffer.from(result[0].picture, 'base64');

    response.writeHead(200, {'Content-Type': 'image/png' });
    response.end(buf,'binary');
  });
});

app.post('/user/loc/:pk', function(request,response){
    const long = request.body.long;
    const lat = request.body.lat;
    var query = "UPDATE items SET";
    query += " latitude="+lat;
    query += " ,longitude="+long;
    query += " WHERE id=(SELECT id FROM users WHERE publickey='"+request.params.pk+"')";
    dbConnection.query(query, function(err, result){
        if(err) throw err;
        if(result.affectedRows){
            response.json('ok');
        }
    });
});


// //Delete movie entry from database
// app.get('/dvd/delete/:movieID', function(request, response){
//
//     console.log("[DELETING ENTRY] Deleted Item " + request.params.movieID);
//
//     dbConnection.query("DELETE FROM movies WHERE movieID='" + request.params.movieID + "'",
//     function(err, result){
//         if(err) throw err;
//         if(result.affectedRows){
//             console.log("[DELETING ENTRY] REDIRECTING]");
//             response.redirect(baseURL);
//         }
//     });
// });
