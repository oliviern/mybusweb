(function ($) {
    'use strict';

    $(document).on('ready', function () {

        // Nice Select
        $('select').niceSelect();
        // -----------------------------
        //  Client Slider
        // -----------------------------
        // $('.category-slider').slick({
        //     slidesToShow:8,
        //     infinite: true,
        //     arrows:false,
        //     autoplay: false,
        //     autoplaySpeed: 2000
        // });
        // -----------------------------
        //  Select Box
        // -----------------------------
        // $('.select-box').selectbox();
        // -----------------------------
        //  Video Replace
        // -----------------------------
        $('.video-box img').click(function() {
            var video = '<iframe allowfullscreen src="' + $(this).attr('data-video') + '"></iframe>';
            $(this).replaceWith(video);
        });
        // -----------------------------
        //  Coupon type Active switch
        // -----------------------------
        $('.coupon-types li').click(function () {
            $('.coupon-types li').not(this).removeClass('active');
            $(this).addClass('active');
        });
        // -----------------------------
        // Datepicker Init
        // -----------------------------
        $('.input-group.date').datepicker({
            format: 'dd/mm/yy'
        });
        // -----------------------------
        // Datepicker Init
        // -----------------------------
        $('#top').click(function() {
          $('html, body').animate({ scrollTop: 0 }, 'slow');
          return false;
        });
        // -----------------------------
        // Button Active Toggle
        // -----------------------------
        $('.btn-group > .btn').click(function(){
            $(this).find('i').toggleClass('btn-active');
        });
        // -----------------------------
        // Coupon Type Select
        // -----------------------------
        $('#online-code').click(function(){
            $('.code-input').fadeIn(500);
        });
        $('#store-coupon, #online-sale').click(function(){
            $('.code-input').fadeOut(500);
        });
        /***ON-LOAD***/
        jQuery(window).on('load', function () {

        });

    });

})(jQuery);



 $(document).ready(function() {
  $('select:not(.ignore)').niceSelect();
});



// GOogle Map

window.marker = null;

function initialize() {
    var map;

    var style = [
    {
        "stylers": [
            {
                "hue": "#ff61a6"
            },
            {
                "visibility": "on"
            },
            {
                "invert_lightness": true
            },
            {
                "saturation": 40
            },
            {
                "lightness": 10
            }
        ]
    }
];

    var mapOptions = {
        // SET THE CENTER
        //center: busloc,

        // SET THE MAP STYLE & ZOOM LEVEL
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        zoom:9,

        // SET THE BACKGROUND COLOUR
        // backgroundColor:"#000",

        // REMOVE ALL THE CONTROLS EXCEPT ZOOM
        zoom:17,
        panControl:false,
        zoomControl:true,
        mapTypeControl:false,
        scaleControl:false,
        streetViewControl:false,
        overviewMapControl:false,
        zoomControlOptions: {
            style:google.maps.ZoomControlStyle.LARGE
        },
        styles: [{"featureType":"administrative.locality","elementType":"all","stylers":[{"hue":"#2c2e33"},{"saturation":7},{"lightness":19},{"visibility":"on"}]},{"featureType":"administrative.locality","elementType":"labels.text","stylers":[{"visibility":"on"},{"saturation":"-3"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"color":"#F07000"}]},{"featureType":"landscape","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"simplified"}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"hue":"#F07000"},{"saturation":"100"},{"lightness":31},{"visibility":"simplified"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#F07000"},{"saturation":"0"}]},{"featureType":"road","elementType":"labels","stylers":[{"hue":"#008eff"},{"saturation":-93},{"lightness":31},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#f3dbc8"},{"saturation":"0"}]},{"featureType":"road.arterial","elementType":"labels","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":-2},{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"hue":"#F07000"},{"saturation":100},{"lightness":-8},{"visibility":"simplified"}]},{"featureType":"transit","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":10},{"lightness":69},{"visibility":"on"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":-78},{"lightness":67},{"visibility":"simplified"}]}]


    }

    map = new google.maps.Map(document.getElementById('map'), mapOptions);

    //CREATE A CUSTOM PIN ICON
    var marker_image ='images/logo simple fond noir.png';
    var pinIcon = new google.maps.MarkerImage(marker_image,null,null, null,new google.maps.Size(42, 42));

    var locations = [
      ['<h3>BUS Stop</h3><img src="images/logo complet noir SMALL.png"></img><br/>Le lieu du bus<br>Welcome !', 45.7708513, 4.8359395],
      ['Coogee Beach', 45.7808513, 4.8459395],
      ['Cronulla Beach', 45.7908513, 4.8399395],
      ['Manly Beach', 45.7608513, 4.8279395],
      ['Maroubra Beach', 45.7508513, 4.8459395]
    ];
    var infowindow = new google.maps.InfoWindow();

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        map.setCenter({
          lat: position.coords.latitude,
          lng: position.coords.longitude
        });

        marker = new google.maps.Marker({
          position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
          url: '/user',
          icon: pinIcon,
          map: map
        });

        // infowindow.setContent("Proposez votre activité ici !");
        // infowindow.open(map, marker);

        google.maps.event.addListener(marker, 'click', function() {
            window.location.href = this.url;
        });

      }, function() {
        //handleLocationError(true, infoWindow, map.getCenter());
      });
    } else {
      // Browser doesn't support Geolocation
      //handleLocationError(false, infoWindow, map.getCenter());
    }

    for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        url: '/single',
        icon: pinIcon,
        map: map
      });

      google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));

      google.maps.event.addListener(marker, 'click', function() {
          window.location.href = this.url;
      });
    }
}

google.maps.event.addDomListener(window, 'load', initialize);




// var slider = new Slider('#ex2', {});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
